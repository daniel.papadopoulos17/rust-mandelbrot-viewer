# Rust Mandelbrot Viewer

This is a Rust project I created to generate images of the [Mandelbrot set](https://en.wikipedia.org/wiki/Mandelbrot_set). Rust was chosen for its performance and support with multithreading. Generating a frame of the Mandelbrot set is computationally expensive and so I wanted to see how quickly this could be accomplished with the use of both Rust and multithreading.

![Mandelbrot Zoom](my_video.mp4)

This project can be used as an interactive viewer of the Mandelbrot set or as a tool to generate videos zooming into the fractal.  This program renders each frame of the Mandelbrot set in 1080p resolution with the use of 12 CPU threads. As an interactive viewer, the program runs in a new window with a preview of the full Mandelbrot set. The user can left click somewhere in the image and a new frame will be generated, zoomed in, and centered at that point. At any time the user can right click and return to the original view. This interactive viewer was also adapted to generate a video zooming into the point `-0.7591323367632493 - 0.07661264649010506i`. All the frames are pre-generated and then can be compiled into a video using the python script `video_stitching.py`.

## Usage
`cargo run --release <still,video>`

`python3 video_stitching.py`
