use ggez::event;
use ggez::graphics::{self};
use ggez::{Context, GameResult};
use ggez::input::mouse::MouseButton;
use glam::*;
use image::{save_buffer_with_format};
use colors_transform::{Rgb, Color, Hsl};
use crossbeam_utils::thread;
use std::env;

const WIDTH: usize = 1920;
const HEIGHT: usize = 1080;
const MAX_ITER: usize = 2000;
const IMAGE_PATH: &str = "target\\release\\resources\\frame.png";
const VIDEO_PATH: &str = "target\\release\\resources\\video\\frame";

fn mandelbrot(i: f64, j: f64) -> Result<usize, String> {
    let mut old_i: f64 = i;
    let mut old_j: f64 = j;
    let mut mag2: f64 = 0.0;
    for n in 0..MAX_ITER {
        if mag2 >= 4.0 {
            return Ok(n);
        }
        let new_i: f64 = old_i*old_i - old_j*old_j + i;
        let new_j: f64 = 2.0*old_i*old_j + j;
        mag2 = new_i*new_i + new_j*new_j;
        old_i = new_i;
        old_j = new_j;
    }
    Ok(MAX_ITER)
}

fn get_colour(i: f64, j: f64) -> Result<Rgb, String> {
    let count = mandelbrot(i, j).unwrap();
    if count == MAX_ITER {
        return Ok(Hsl::from(0.0, 0.0, 0.0).to_rgb());
    }
    let h = ((count % 90) * 4) as f32;
    Ok(Hsl::from(h, 100.0, 50.0).to_rgb())
}

fn generate_frame(x_center: f64, y_center: f64, frame_width: f64, path: &str) -> Result<(), String> {
    let num_threads = 12;
    let frame_height = frame_width * (HEIGHT as f64 / WIDTH as f64) as f64;
    let y_zone_height = HEIGHT / num_threads;
    let mut pixels = Vec::new();
    thread::scope(|scope| {
        let mut handles = Vec::new();
        for thread in 0..num_threads {
            let offset = thread * y_zone_height;
            handles.push(scope.spawn(move |_| {
                let mut result = Vec::new();
                for y_rel in 0..y_zone_height {
                    let y = y_rel + offset;
                    for x in 0..WIDTH {
                        let i = frame_width*((x as f64 / WIDTH as f64) - 0.5) + x_center;
                        let j = frame_height*((y as f64 / HEIGHT as f64) - 0.5) + y_center;
                        let rgb = get_colour(i, j).unwrap();
                        result.push(rgb.get_red() as u8);
                        result.push(rgb.get_green() as u8);
                        result.push(rgb.get_blue() as u8);
                    }
                }
                result
            }));
        }

        for handle in handles {
            let section = handle.join().unwrap();
            for it in section {
                pixels.push(it);
            }
        }
        save_buffer_with_format(path, &pixels, WIDTH as u32, HEIGHT as u32, image::ColorType::Rgb8, image::ImageFormat::Png).unwrap();
    }).unwrap();
    Ok(())
}

struct MainState {
    image: graphics::Image,
    cur_x: f64,
    cur_y: f64,
    cur_width: f64,
    generate_video: bool,
    cur_frame: u32
}

impl MainState {
    fn new(ctx: &mut Context, generate_video: bool) -> GameResult<MainState> {
        let im = graphics::Image::new(ctx, "/frame.png")?;
        let s = MainState {
            image: im,
            cur_x: -0.5,
            cur_y: 0.0,
            cur_width: 6.0,
            generate_video: generate_video,
            cur_frame: 0
        };
        generate_frame(-0.5, 0.0, 0.01, IMAGE_PATH).unwrap();
        Ok(s)
    }
}

impl event::EventHandler<ggez::GameError> for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        if self.generate_video {
            let width = self.cur_width * 0.95;
            let full_path = format!("{}{}.png", VIDEO_PATH, self.cur_frame);
            generate_frame(-0.7591323367632493, -0.07661264649010506, width, &full_path[..]).unwrap();
            let path = format!("/video/frame{}.png", self.cur_frame);
            self.image = graphics::Image::new(ctx, path)?;
            self.cur_width = width;
            self.cur_frame = self.cur_frame + 1;

            if width < 0.000000000000001 {
                std::process::exit(1);
            }
        }
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, [0.1, 0.2, 0.3, 1.0].into());

        graphics::draw(ctx, &self.image, (glam::Vec2::new(0.0, 0.0),))?;

        graphics::present(ctx)?;
        Ok(())
    }

    fn mouse_button_down_event(&mut self, ctx: &mut Context, button: MouseButton, x: f32, y: f32) {
        if !self.generate_video {
            if button == MouseButton::Left {
                let new_x = ((x as f64) / (WIDTH as f64) - 0.5) * self.cur_width + self.cur_x;
                let new_y = ((y as f64) / (HEIGHT as f64) - 0.5) * self.cur_width * (HEIGHT as f64 / WIDTH as f64) + self.cur_y;
                let new_width = self.cur_width * 0.4;
    
                generate_frame(new_x, new_y, new_width, IMAGE_PATH).unwrap();
                self.cur_x = new_x;
                self.cur_y = new_y;
                self.cur_width = new_width;
    
                let im = graphics::Image::new(ctx, "/frame.png").unwrap();
                self.image = im;

                println!("{}, {}", new_x, new_y);
            } else if button == MouseButton::Right {
                generate_frame(-0.5, 0.0, 6.0, IMAGE_PATH).unwrap();
                self.cur_x = -0.5;
                self.cur_y = 0.0;
                self.cur_width = 6.0;
    
                let im = graphics::Image::new(ctx, "/frame.png").unwrap();
                self.image = im;
            }
        }
    }
}

pub fn main() -> GameResult {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Usage: [still,video]");
        return Ok(());
    }
    let generate_video = match &*args[1] {
        "still" => false,
        "video" => true,
        _ => {
            eprintln!("Specify either \"still\" or \"video\"");
            return Ok(());
        }
    };

    let cb = ggez::ContextBuilder::new("Mandelbrot", "Daniel Papadopoulos")
        .window_mode(ggez::conf::WindowMode::default()
            .dimensions(WIDTH as f32, HEIGHT as f32))
        .window_setup(ggez::conf::WindowSetup::default()
            .title("Mandelbrot Viewer"));
    let (mut ctx, event_loop) = cb.build()?;
    let state = MainState::new(&mut ctx, generate_video)?;
    event::run(ctx, event_loop, state)
}
